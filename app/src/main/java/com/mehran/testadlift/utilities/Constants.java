package com.mehran.testadlift.utilities;

/**
 * Created by Mehran on 2/6/2018.
 */

public class Constants {
    public static final String BASE_URL = "https://api.adlift.ir/api/test/";
    public static final int IS_SUCCESSFUL = 200;
    public static final int FORBIDDEN = 403;
    public static final int NOT_FOUND = 404;

}
