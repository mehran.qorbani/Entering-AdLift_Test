package com.mehran.testadlift.utilities;

import android.app.Application;
import android.content.Context;
import android.graphics.Typeface;

public class MyApplication extends Application {

    public static Typeface font;
    public static String VERSION_NAME;
    public static String PACKAGE_NAME;
    public static Integer COUNTER;
    private static MyApplication ourInstance;
    private Context context;

    public static synchronized MyApplication getInstance() {
        return ourInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        ourInstance = this;
        context = getApplicationContext();

    }

    public Context getAppContext() {
        return context;
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }
}
