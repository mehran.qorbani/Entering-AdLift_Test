package com.mehran.testadlift.utilities;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by Mehran on 2/6/2018.
 */

public class SP {


    public static final String PREF_FILE_NAME = "testAdlift";

    public static String rsfp(Context context, String preferencesName, String defaultValue) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString(preferencesName, defaultValue);
    }

    public static void sstp(Context context, String preferencesName, String preferencesValue) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(preferencesName, preferencesValue);
        editor.apply();
    }

    public static int rifp(Context context, String preferencesName, int defaultValue) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getInt(preferencesName, defaultValue);
    }

    public static void sitp(Context context, String preferencesName, int preferencesValue) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(preferencesName, preferencesValue);
        editor.apply();
    }

    public static boolean rbfp(Context context, String preferencesName, boolean defaultValue) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getBoolean(preferencesName, defaultValue);
    }

    public static void sbtp(Context context, String preferencesName, boolean preferencesValue) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(preferencesName, preferencesValue);
        editor.apply();
    }

    public static void rfp(Context context, String preferencesName) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.remove(preferencesName);
        editor.apply();
    }

}
