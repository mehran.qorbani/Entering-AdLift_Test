package com.mehran.testadlift.utilities;

import android.content.Context;
import android.net.ConnectivityManager;

public class NetworkUtility {
    public static boolean isInternetConnectionAvailable(Context context) {
        final ConnectivityManager connectivityManager = ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE));
        return (connectivityManager != null ? connectivityManager.getActiveNetworkInfo() : null) != null && connectivityManager.getActiveNetworkInfo().isConnected();
    }
}
