package com.mehran.testadlift.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.mehran.testadlift.model.TripList;

import java.util.List;

/**
 * Created by Mehran on 2/6/2018.
 */

public class SqliteDBHandler extends SQLiteOpenHelper {
    String tblQuery = "" +
            " CREATE TABLE tripList ( " +
            "  _id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL ,  " +
            " start TEXT ," +
            " end TEXT ," +
            " time TEXT" +
            " ) ";


    public SqliteDBHandler(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(tblQuery);
    }

    public void insetTripList(List<TripList> tripList) {
        SQLiteDatabase db = this.getWritableDatabase();
        for (TripList s : tripList) {

            String insertQuery = "" +
                    "   INSERT INTO tripList (start , end ,time ) " +
                    "  VALUES(  '" + s.getStart() + " '  , '" + s.getEnd() + "','"+s.getTime()+"'   ) ";
            db.execSQL(insertQuery);

        }

        db.close();
    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
