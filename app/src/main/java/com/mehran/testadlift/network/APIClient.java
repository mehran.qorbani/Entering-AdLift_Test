package com.mehran.testadlift.network;

import android.app.Activity;

import com.mehran.testadlift.model.Login;
import com.mehran.testadlift.model.TripList;
import com.mehran.testadlift.utilities.Constants;
import com.mehran.testadlift.utilities.NetworkUtility;

import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Mehran on 2/6/2018.
 */

public class APIClient {
    private static APIClient instance;
    private APIService apiService;

    private APIClient() {
        OkHttpClient client = new OkHttpClient.Builder()
                .readTimeout(120, TimeUnit.SECONDS)
                .writeTimeout(120, TimeUnit.SECONDS)
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();
        apiService = retrofit.create(APIService.class);
    }


    public static APIClient getInstance() {
        if (instance == null) {
            instance = new APIClient();
        }
        return instance;
    }

    public Call<Login> login(String username, String password) {
        return apiService.login(username, password);

    }

    public Call<List<TripList>> getTrip(String token) {
        return apiService.getTripList(token);
    }

}
