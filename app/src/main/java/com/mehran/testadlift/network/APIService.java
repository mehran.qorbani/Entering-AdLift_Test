package com.mehran.testadlift.network;

import com.mehran.testadlift.model.Login;
import com.mehran.testadlift.model.TripList;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by Mehran on 2/6/2018.
 */

public interface APIService {
    @POST("login")
    @FormUrlEncoded
    Call<Login> login(@Field("username") String username
            , @Field("password") String password
    );

    @GET("trips/{TOKEN}")
    Call<List<TripList>> getTripList(@Path("TOKEN") String token);

}
