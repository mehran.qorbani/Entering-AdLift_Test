package com.mehran.testadlift.view;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.Toast;

import com.mehran.testadlift.R;
import com.mehran.testadlift.model.Login;
import com.mehran.testadlift.network.APIClient;
import com.mehran.testadlift.utilities.Constants;
import com.mehran.testadlift.utilities.SP;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {
    EditText username, password;
    Button loginBtn;
    String token;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        setUi();
        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validate()) {
                    login();
                }
            }
        });

    }

    private void setUi() {
        username = findViewById(R.id.usernameET);
        password = findViewById(R.id.passwordET);
        loginBtn = findViewById(R.id.btn_login);
    }

    private void login() {

        APIClient.getInstance()
                .login(username.getText().toString(), password.getText().toString()).enqueue(new Callback<Login>() {
            @Override
            public void onResponse(Call<Login> call, Response<Login> response) {
                switch (response.code()) {
                    case Constants.IS_SUCCESSFUL:
                        SP.sstp(LoginActivity.this, "token", response.body().getToken());
                        startActivity(new Intent(LoginActivity.this, MainActivity.class));
                        finish();
                        break;
                    case Constants.FORBIDDEN:
                        Toast.makeText(LoginActivity.this, "نام کاربری یا رمز عبور استباه است", Toast.LENGTH_SHORT).show();
                        break;
                }
            }

            @Override
            public void onFailure(Call<Login> call, Throwable t) {
                Toast.makeText(LoginActivity.this, "check your Internet connection", Toast.LENGTH_SHORT).show();
            }
        });

    }

    //check if inputs are valid
    public boolean validate() {
        boolean valid = true;

        if (username.getText().toString().isEmpty()) {
            username.setError(getString(R.string.username_empty));
            valid = false;
        } else {
            username.setError(null);
        }

        if (password.getText().toString().isEmpty()) {
            password.setError(getString(R.string.password_empty));
            valid = false;
        } else {
            password.setError(null);
        }
        return valid;
    }

}
