package com.mehran.testadlift.view;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.mehran.testadlift.utilities.SP;

public class MainEmptyActivity extends AppCompatActivity {
    Intent activityIntent;
    String tokenChecker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        tokenChecker = SP.rsfp(this, "token", null);

        //check if token exist
        if (!(tokenChecker ==null)) {
            activityIntent = new Intent(MainEmptyActivity.this, MainActivity.class);
        } else {
            activityIntent = new Intent(MainEmptyActivity.this, LoginActivity.class);

        }
        startActivity(activityIntent);
        finish();

    }
}
