package com.mehran.testadlift.view;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.mehran.testadlift.R;
import com.mehran.testadlift.adaptor.TripListAdapter;
import com.mehran.testadlift.db.SqliteDBHandler;
import com.mehran.testadlift.model.TripList;
import com.mehran.testadlift.network.APIClient;
import com.mehran.testadlift.utilities.Constants;
import com.mehran.testadlift.utilities.SP;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
    Toolbar toolbar;
    Button logoutBtn;
    TextView noTrip;
    RecyclerView recyclerView;
    String token;
    TripListAdapter mAdapter;
    List<TripList> tripLists = new ArrayList<>();
    SqliteDBHandler db;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setUpToolbar();
        setUI();
        token = SP.rsfp(MainActivity.this, "token", null);
        loadTrips();

        logoutBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tokenExpire();

            }
        });

    }


    private void setUI() {
        recyclerView = findViewById(R.id.recyclerViewTrips);
        logoutBtn = findViewById(R.id.loguotBTN);
        noTrip=findViewById(R.id.no_trip_found);
    }

    private void setUpToolbar() {
        toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("");
        ActionBar actionBar = getSupportActionBar();
    }


    private void loadTrips() {
        APIClient.getInstance().getTrip(token).enqueue(new Callback<List<TripList>>() {
            @Override
            public void onResponse(Call<List<TripList>> call, Response<List<TripList>> response) {
                switch (response.code()) {
                    case Constants.IS_SUCCESSFUL:
                        tripLists = response.body();
                        setupRV(tripLists);
                        saveToDB(tripLists);

                        break;
                    case Constants.FORBIDDEN:
                        startActivity(new Intent(MainActivity.this, LoginActivity.class));
                        finish();
                        break;
                    case Constants.NOT_FOUND:
                         noTrip.setVisibility(View.VISIBLE);
                        break;
                }
            }

            @Override
            public void onFailure(Call<List<TripList>> call, Throwable t) {

                Log.d("tetest", t.getLocalizedMessage() + "");
            }
        });


    }

    private void saveToDB(List<TripList> tripLists) {
            db=new SqliteDBHandler(this,"trip.db",null,1);
            db.insetTripList(tripLists);

    }

    private void setupRV(List<TripList> response) {
        LinearLayoutManager llm = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        mAdapter = new TripListAdapter(this, response);
        recyclerView.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();
        recyclerView.setLayoutManager(llm);


    }

    private void tokenExpire() {
        SP.rfp(this, "token");
        startActivity(new Intent(MainActivity.this, LoginActivity.class));
        finish();

    }
}
