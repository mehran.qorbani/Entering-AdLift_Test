package com.mehran.testadlift.adaptor;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mehran.testadlift.R;
import com.mehran.testadlift.model.TripList;

import org.w3c.dom.Text;

import java.util.Date;
import java.util.List;

import retrofit2.Response;

/**
 * Created by Mehran on 2/6/2018.
 */

public class TripListAdapter extends RecyclerView.Adapter<TripListAdapter.MyViewHolder> {
    Context mContext;
    List<TripList> mList;

    public TripListAdapter(Context mContext, List<TripList> mList) {
        this.mContext = mContext;
        this.mList = mList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.trip_list_item, parent, false);

        return new MyViewHolder(view);

    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        long time = Long.parseLong(mList.get(position).getTime());
        Date date = new Date();
        date.setTime((long) time * 1000);


        holder.tripId.setText(Integer.toString(mList.get(position).getTripId()));
        holder.tripStart.setText(Integer.toString(mList.get(position).getStart()));
        holder.tripEnd.setText(Integer.toString(mList.get(position).getEnd()));
        holder.tripTime.setText(date.toString());

    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tripId, tripStart, tripEnd, tripTime;

        public MyViewHolder(View itemView) {
            super(itemView);
            tripId = itemView.findViewById(R.id.trip_id);
            tripStart = itemView.findViewById(R.id.trip_start);
            tripEnd = itemView.findViewById(R.id.trip_end);
            tripTime = itemView.findViewById(R.id.trip_time);
        }
    }
}
